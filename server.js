'use strict';

//Lets require/import the HTTP module
var http = require('http');
var KlikAanKlikUit = require('kaku-rpi');

var kaku = KlikAanKlikUit(11);//,260,7,5
var address = 4245018;

//Lets define a port we want to listen to
var PORT = 3737;

var units = [
    {name: 'Jij', on: false, color: {red: 255, green: 255, blue: 255}},
    {name: 'bent', on: false, color: {red: 255, green: 255, blue: 255}},
    {name: 'de', on: false, color: {red: 255, green: 255, blue: 255}},
    {name: 'liefste', on: false, color: {red: 255, green: 255, blue: 255}},
];

//We need a function which handles requests and send response
function handleRequest(request, response) {
    if (request.url == '/huebridgesimulator/currentMenu') {
        response.statusCode = 200;
        response.setHeader('Content-Type', 'application/json');
        response.write(JSON.stringify(units));
        response.end();
    } else if (request.url.match('/huebridgesimulator/menuItemClicked/')) {
        var urlString = request.url.replace('/huebridgesimulator/menuItemClicked/', '');
        var name = urlString.substr(0, urlString.indexOf('/'));
        var state = urlString.substr(name.length + 1);
        for (var i in units) {
            if (units[i].name == name) {
                units[i].on = state == 'true';
                console.log( i, units[i].on );
                if( units[i].on ){
                    kaku.on(address, i);
                    setTimeout(function(){kaku.on(address, i);},500);
                } else {
                    kaku.off(address, i);
                    setTimeout(function(){kaku.off(address, i);},500);
                }
            }
        }
        response.end('oke');
    } else {
        response.end('unhandled: ' + request.url);
        console.log(request.url);
    }

}

var server = http.createServer(handleRequest);

server.listen(PORT, function () {
    console.log("Server listening on: http://localhost:%s", PORT);
});